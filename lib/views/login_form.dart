import 'package:flutter/material.dart';
import 'package:xlms_client/controllers/sakai_controller.dart';
import 'package:xlms_client/views/course_screen.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginFormState();
  }
}

class LoginFormState extends State<LoginForm> {
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  final SakaiController _controller = SakaiController();

  String authMessage = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            Container(
              margin: EdgeInsets.only(top: 10),
            ),
            passwordField(),
            Container(
              margin: EdgeInsets.only(top: 10),
            ),
            loginButton(),
            Text(authMessage),
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return StreamBuilder(
      // stream: bloc.streamEmail,
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        return TextFormField(
          controller: emailController,
          keyboardType: TextInputType.emailAddress,
          decoration: const InputDecoration(
            icon: Icon(Icons.person),
            labelText: 'Email address',
          ),
        );
      },
    );
  }

  Widget passwordField() {
    return TextFormField(
      controller: passwordController,
      obscureText: true,
      decoration: const InputDecoration(
        icon: Icon(Icons.password),
        labelText: 'Password',
      ),
    );
  }

  Widget loginButton() {
    return ElevatedButton(
      onPressed: () async {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          final email = emailController.text;
          final password = passwordController.text;
          print('Login request: $email - $password');

          dynamic authResponse =
              await _controller.authenticate(email, password);
          print(authResponse.statusCode);

          List<String> enrolledCourses = await _controller.getSites();
          print(enrolledCourses);
          if (authResponse.statusCode == 201 ||
              authResponse.statusCode == 200) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => CourseScreen(enrolledCourses)));
            authMessage = '';
            setState(() {});
          } else {
            authMessage = 'Authentication failed.';
            setState(() {});
          }
        }
      },
      child: const Text('Login'),
    );
  }
}
