import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'views/app.dart';

void main() async {
  HttpOverrides.global = MyHttpOverrides(); // to fix a certificate error
  runApp(xlmsClientApp());
}

class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext? context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }

}


